import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContraloriaComponent } from './pages/contraloria/contraloria.component';
import { NavComponent } from './components/nav/nav.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    ContraloriaComponent,
    NavComponent,
  
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,   
    AsesorComexModule

  ],
  exports:[
    ContraloriaComponent
  ]
})
export class ContraloriaModule { }
