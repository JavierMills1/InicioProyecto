import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  fecha: Date = new Date();

  mostrarSolicitudAutomatica: boolean = false;
  mostrarSolicitudHotelero: boolean = false;

  constructor() {}

  ngOnInit(): void {}
  
  inicio() {
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudHotelero = false;
  }

  mostrarAutomatica() {
    this.mostrarSolicitudAutomatica = true;
    this.mostrarSolicitudHotelero = false;
  }

}

