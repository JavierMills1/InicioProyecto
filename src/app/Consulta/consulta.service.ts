import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StockBancomext } from '../Models/TStock';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  public url = "https://my-json-server.typicode.com/JavierMills";

  constructor( private http: HttpClient) { }

// APIS hacia las data tables

  getDataTableStock(): Observable<StockBancomext[]>{
  
    return this.http.get<StockBancomext[]>(`${this.url}/consumirAPIpublica/Stock_Bancomext`);

  }
}
