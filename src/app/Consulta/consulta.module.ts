import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { StockBancomextComponent } from './components/stock-bancomext/stock-bancomext.component';
import { SharedModule } from '../shared/shared.module';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { TableStockComponent } from './Tables/table-stock/table-stock.component';
import { GarantiasModule } from '../Garantias/garantias.module';



@NgModule({
  declarations: [
    NavComponent,
    StockBancomextComponent,
    ConsultaComponent,
    TableStockComponent,
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    FormsModule,
    SharedModule,
    AsesorComexModule,
    RouterModule,
    GarantiasModule
  ]
})
export class ConsultaModule { }
