import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { StockBancomext } from 'src/app/Models/TStock';
import { ConsultaService } from '../../consulta.service';

@Component({
  selector: 'app-table-stock',
  templateUrl: './table-stock.component.html',
  styleUrls: ['./table-stock.component.scss']
})
export class TableStockComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();



  dataStockBancomext: StockBancomext []= [];
  TablasBolsasEnTramite = false;
  constructor(private tableService:ConsultaService) { }

  ngOnInit(): void {
    this.getDataTable();
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 40,
      };

  }

  getDataTable() {
    this.tableService.getDataTableStock().subscribe((resp) => {
      this.dataStockBancomext = resp;
      this.dtTrigger.next(0);
    });
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
