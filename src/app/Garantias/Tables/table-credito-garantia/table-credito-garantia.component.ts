import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { CreditoGarantia } from 'src/app/Models/CreditoEnGarantia';
import { ServicesService } from '../../services/services.service';

@Component({
  selector: 'app-table-credito-garantia',
  templateUrl: './table-credito-garantia.component.html',
  styleUrls: ['./table-credito-garantia.component.scss']
})
export class TableCreditoGarantiaComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataCreditoGarantia: CreditoGarantia []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableCreditoProgramas: ServicesService) {}

  ngOnInit(): void {
    this.getDataTableCatalogoProgramas();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableCatalogoProgramas() {
    this.tableCreditoProgramas.getTableCreditoEnGarantia().subscribe((resp) => {
      this.dataCreditoGarantia = resp;
      console.log(this.dataCreditoGarantia);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }
}
