import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { CatalogoProgramas } from 'src/app/Models/CatalogoProgramas';
import { ServicesService } from '../../services/services.service';

@Component({
  selector: 'app-table-catalogo-programas',
  templateUrl: './table-catalogo-programas.component.html',
  styleUrls: ['./table-catalogo-programas.component.scss']
})
export class TableCatalogoProgramasComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataCatalogoProgramas: CatalogoProgramas []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableCatalogoProgramas: ServicesService) {}

  ngOnInit(): void {
    this.getDataTableCatalogoProgramas();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableCatalogoProgramas() {
    this.tableCatalogoProgramas.getTableCatalogoProgramas().subscribe((resp) => {
      this.dataCatalogoProgramas = resp;
      console.log(this.dataCatalogoProgramas);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

}
