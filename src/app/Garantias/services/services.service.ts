import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CatalogoProgramas } from 'src/app/Models/CatalogoProgramas';
import { CreditoGarantia } from 'src/app/Models/CreditoEnGarantia';

@Injectable({
  providedIn: 'root',
})
export class ServicesService {
  public url = 'https://my-json-server.typicode.com/JavierMills';

  constructor(private http: HttpClient) {}

  getTableCatalogoProgramas(): Observable<CatalogoProgramas[]> {
    return this.http.get<CatalogoProgramas[]>(`${this.url}/deployNtfs/CatalogoProgramas`
    );
  }

  getTableCreditoEnGarantia(): Observable<CreditoGarantia[]> {
    return this.http.get<CreditoGarantia[]>(`${this.url}/Bellari/CreditoEnGarantia`
    );
  }
}

