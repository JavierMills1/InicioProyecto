import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AltaProgramaComponent } from './components/alta-programa/alta-programa.component';
import { NavComponent } from './components/nav/nav.component';
import { CatalogoProgramasComponent } from './components/catalogo-programas/catalogo-programas.component';
import { CreditogtiaComponent } from './components/creditogtia/creditogtia.component';
import { GtiaClienteComponent } from './components/gtia-cliente/gtia-cliente.component';
import { RouterModule, ɵassignExtraOptionsToRouter } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { GarantiasComponent } from './pages/garantias/garantias.component';
import { TableSeleccionComponent } from './components/table-seleccion/table-seleccion.component';
import { TableCatalogoProgramasComponent } from './Tables/table-catalogo-programas/table-catalogo-programas.component';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { TableCreditoGarantiaComponent } from './Tables/table-credito-garantia/table-credito-garantia.component';


@NgModule({
  declarations: [
    AltaProgramaComponent,
    NavComponent,
    CatalogoProgramasComponent,
    CreditogtiaComponent,
    GtiaClienteComponent,
    GarantiasComponent,
    TableSeleccionComponent,
    TableCatalogoProgramasComponent,
    TableCreditoGarantiaComponent,
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    SharedModule,
    RouterModule,
    AsesorComexModule
  ],
  exports:[
    AltaProgramaComponent,
    TableSeleccionComponent,
    CreditogtiaComponent,
    TableCreditoGarantiaComponent
  ]
})
export class GarantiasModule { }
