import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarteraComponent } from './pages/cartera/cartera.component';
import { NavComponent } from './components/nav/nav.component';
import { SharedModule } from '../shared/shared.module';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';



@NgModule({
  declarations: [
    CarteraComponent,
    NavComponent,
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    FormsModule,
    SharedModule,
    AsesorComexModule,
    RouterModule
  ],
  exports:[
    CarteraComponent
  ]
})
export class CarteraModule { }
