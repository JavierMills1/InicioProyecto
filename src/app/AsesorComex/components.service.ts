import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Catalogo } from '../Models/Catalogo';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {


  private urlBase = "https://my-json-server.typicode.com/JavierMills"

  

  constructor(private http: HttpClient) { }

  obtenerCatalogoEstatus() : Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(`${this.urlBase}/AjaxVanila/estatus`)
  }
}
