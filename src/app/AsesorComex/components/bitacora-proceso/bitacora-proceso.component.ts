import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ClienteBusqueda } from 'src/app/Models/ClienteBusqueda';

@Component({
  selector: 'app-bitacora-proceso',
  templateUrl: './bitacora-proceso.component.html',
  styleUrls: ['./bitacora-proceso.component.scss']
})
export class BitacoraProcesoComponente implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  allUsers: any = [];

  constructor(private http: HttpClient) { }


  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40
    };

    this.http.get<ClienteBusqueda>("https://my-json-server.typicode.com/JavierMills/Paises/clientes")
    .subscribe( resp => {
      this.allUsers = resp;
      this.dtTrigger.next(0);
    })

  }

ngOnDestroy(): void {
  this.dtTrigger.unsubscribe();
}
}
