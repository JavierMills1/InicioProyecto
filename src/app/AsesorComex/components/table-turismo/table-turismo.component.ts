import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TableService } from '../table/table.service';
import { SteperService } from '../steper/steper.service';

@Component({
  selector: 'app-table-turismo',
  templateUrl: './table-turismo.component.html',
  styleUrls: ['./table-turismo.component.scss']
})
export class TableTurismoComponent implements OnInit, OnDestroy {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  allUsers: any = [];
  allUsersComplete: any = [];

  constructor(private http: HttpClient, private steperService:SteperService, private tableService:TableService) { }

  ngOnInit(): void {
    this.getDataTable();

      this.tableService.getFiltroStatus().subscribe((data) => {
       this.allUsers = this.allUsersComplete.filter((item:any) => item.status == data);      
      });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40
    };

  }

  getDataTable() {
    this.tableService.getDataTable().subscribe((resp) => {
      this.allUsers = resp;
      this.allUsersComplete = resp;
      console.log(this.allUsers);
      this.dtTrigger.next(0);
    });
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  
  setMenu(status:any){
    this.steperService.setActive(this.steperService.getPasoByDescripcion(status) ?? "");
    this.steperService.setMenuOrigen("TablaAutomaticas");
  }

}
