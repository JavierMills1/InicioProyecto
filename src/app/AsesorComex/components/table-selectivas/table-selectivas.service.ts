import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, Observable } from 'rxjs';
import { ClienteBusquedaSelectivas } from 'src/app/Models/ClienteSelectivas';
@Injectable({
  providedIn: 'root'
})
export class TableSelectivasService {

  filtroStatus$ = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }

  setFiltroStatus(filter:any){    
    this.filtroStatus$.next(filter);
  }

  getTableSelectivas() : Observable<ClienteBusquedaSelectivas>{
    return this.http.get<ClienteBusquedaSelectivas>("https://my-json-server.typicode.com/JavierMills/NFTs/Selectivas");

  }


  getFiltroStatus():Observable<any>{
    return this.filtroStatus$;
   }
}
