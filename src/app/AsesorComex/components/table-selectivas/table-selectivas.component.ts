import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SteperService } from '../steper/steper.service';
import { TableSelectivasService } from './table-selectivas.service';

@Component({
  selector: 'app-table-selectivas',
  templateUrl: './table-selectivas.component.html',
  styleUrls: ['./table-selectivas.component.scss']
})
export class TableSelectivasComponent implements OnInit, OnDestroy {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  allUsers: any = [];
  allUsersComplete: any = [];

  constructor(private http: HttpClient, private steperService:SteperService, private tableSelectivasService:TableSelectivasService) { }

  ngOnInit(): void {
    this.getDataTable();
      this.tableSelectivasService.getFiltroStatus().subscribe((data) => {        
      this.allUsers = this.allUsersComplete.filter((item:any) => item.status == data);       
      });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40
    };

  }

  getDataTable() {
    this.tableSelectivasService.getTableSelectivas().subscribe((resp) => {
      this.allUsers = resp;
      console.log(this.allUsers);
      this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  
ngOnDestroy(): void {
  this.dtTrigger.unsubscribe();
}

setMenu(status:any){
  this.steperService.setActiveSelectivas(this.steperService.getPasoByDescripcionSelectiva(status) ?? "");
  this.steperService.setMenuOrigen("Selectivas");
}
}
