import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { TableService } from '../table/table.service';
import { SteperService } from '../steper/steper.service';

@Component({
  selector: 'app-table-comex-tmec',
  templateUrl: './table-comex-tmec.component.html',
  styleUrls: ['./table-comex-tmec.component.scss']
})
export class TableComexTmecComponent implements OnInit, OnDestroy {


  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  allUsers: any = [];
  allUsersComplete: any = [];

  constructor(private steperService:SteperService, private tableService:TableService) { }

  ngOnInit(): void {
    this.getDataTable();

      this.tableService.getFiltroStatus().subscribe((data) => {
       this.allUsers = this.allUsersComplete.filter((item:any) => item.status == data);      
      });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40
    };

  }

  getDataTable() {
    this.tableService.getDataTable().subscribe((resp) => {
      this.allUsers = resp;
      this.allUsersComplete = resp;
      console.log(this.allUsers);
      this.dtTrigger.next(0);
    });
  }
  
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  
  setMenu(status:any){
    this.steperService.setActive(this.steperService.getPasoByDescripcion(status) ?? "");
    this.steperService.setMenuOrigen("TablaAutomaticas");
  }


}
