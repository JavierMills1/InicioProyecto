import { SteperService } from '../../steper/steper.service';
import { TableService } from '../../table/table.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ComponentsService } from 'src/app/AsesorComex/components.service';
import { Catalogo } from 'src/app/Models/Catalogo';

@Component({
  selector: 'app-filtro-automaticas',
  templateUrl: './filtro-automaticas.component.html',
  styleUrls: ['./filtro-automaticas.component.scss']
})
export class FiltroAutomaticasComponent implements OnInit {
  data: Catalogo []= [];

  filtrosForm = this.formBuilder.group({
    buc: '',
    cliente: '',
    banca: '',
    estatus: '',
    sucursal: '',
    region: '',
    zona: ''
  });

  mostrarTablaAutomaticas: boolean = false;
  mostrarSolicitudAutomatica: boolean = false;

  public CapturarSolicitud: string = 'Capturar Solcitud';
  public ProcesoPendienteAsesorC: string = 'Proceso Pendiente Asesor Comex';
  public EspecialistaComexRech: string = 'Especialista Comex (Rechazos)';
  public ProcesoPendienteContraloria: string = 'Proceso Pendiente Contraloría';
  public ProcesoPendienteCartera: string = 'Proceso Pendiente Cartera';
  public FondosDeFomento: string = 'Fondos de Fomento';
  public EnviadaNafinet: string = 'Enviada a Nafinet';
  public AprobadoNafinet: string = 'Aprobado Nafinet';
  public RechaxoNafinet: string = 'Rechazo Nafinet';
  public Reproceso: string = 'Reproceso';
 
  pasoDEfault = "paso1";

  constructor(private steperService:SteperService, 
    private tableService:TableService, private formBuilder:FormBuilder, private pasos: ComponentsService) {

  }
  ngOnInit(): void {
this.getDataTableBitacoraBolsas()

    this.steperService.setActive(this.pasoDEfault);
  }
  getDataTableBitacoraBolsas() {
    this.pasos.obtenerCatalogoEstatus().subscribe((resp) => {
      this.data = resp;
      console.log(resp);
    });
  }


  mostrarTablasAutomaticas() {
    this.tableService.setFiltroStatus(this.steperService.getDescripcion(this.pasoDEfault));
    this.mostrarTablaAutomaticas = true;
  }
  limpiar(){
    this.filtrosForm.reset();
  }
  submit(){

  }

}
