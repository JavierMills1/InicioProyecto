import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, Observable, Subject } from 'rxjs';
import { ClienteBusqueda } from 'src/app/Models/ClienteBusqueda';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TableService {

  
  filtroStatus$ = new BehaviorSubject(null);

  constructor( private http: HttpClient) { }



  getDataTable(): Observable<ClienteBusqueda> {
    
    const url = "https://my-json-server.typicode.com/JavierMills";

    return this.http.get<ClienteBusqueda>(`${url}/Paises/clientes`);
    
  }

 
  setFiltroStatus(filter:any){
    console.log("setFiltroStatus: ", filter);
    
    this.filtroStatus$.next(filter);
  }

  getFiltroStatus():Observable<any>{
    return this.filtroStatus$;
   }
}
