import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productos-asociados',
  templateUrl: './productos-asociados.component.html',
  styleUrls: ['./productos-asociados.component.scss']
})
export class ProductosAsociadosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
