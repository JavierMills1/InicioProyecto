import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crear-reporte-ejecutivo',
  templateUrl: './crear-reporte-ejecutivo.component.html',
  styleUrls: ['./crear-reporte-ejecutivo.component.scss']
})
export class CrearReporteEjecutivoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
