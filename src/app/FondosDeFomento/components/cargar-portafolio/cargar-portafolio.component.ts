import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cargar-portafolio',
  templateUrl: './cargar-portafolio.component.html',
  styleUrls: ['./cargar-portafolio.component.scss']
})
export class CargarPortafolioComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
