import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sincronizar-sig-vs-extra',
  templateUrl: './sincronizar-sig-vs-extra.component.html',
  styleUrls: ['./sincronizar-sig-vs-extra.component.scss']
})
export class SincronizarSigVsExtraComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    alert("¿Esta seguro de sincronizar garantias con extra?");
  }

}
