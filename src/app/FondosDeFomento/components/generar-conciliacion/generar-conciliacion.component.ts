import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generar-conciliacion',
  templateUrl: './generar-conciliacion.component.html',
  styleUrls: ['./generar-conciliacion.component.scss']
})
export class GenerarConciliacionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
