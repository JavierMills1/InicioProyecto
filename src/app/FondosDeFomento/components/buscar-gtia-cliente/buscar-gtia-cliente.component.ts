import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buscar-gtia-cliente',
  templateUrl: './buscar-gtia-cliente.component.html',
  styleUrls: ['./buscar-gtia-cliente.component.scss']
})
export class BuscarGtiaClienteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
