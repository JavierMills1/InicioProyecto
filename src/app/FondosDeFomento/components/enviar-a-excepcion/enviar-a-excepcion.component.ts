import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enviar-a-excepcion',
  templateUrl: './enviar-a-excepcion.component.html',
  styleUrls: ['./enviar-a-excepcion.component.scss']
})
export class EnviarAExcepcionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    alert("Hola Mundo");
  }

}
