import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generar-reporte-bmx',
  templateUrl: './generar-reporte-bmx.component.html',
  styleUrls: ['./generar-reporte-bmx.component.scss']
})
export class GenerarReporteBmxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
}
}
