import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generar-polizas-contables',
  templateUrl: './generar-polizas-contables.component.html',
  styleUrls: ['./generar-polizas-contables.component.scss']
})
export class GenerarPolizasContablesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
