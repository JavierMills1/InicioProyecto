import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockGarantiasSelectivasComponent } from './stock-garantias-selectivas.component';

describe('StockGarantiasSelectivasComponent', () => {
  let component: StockGarantiasSelectivasComponent;
  let fixture: ComponentFixture<StockGarantiasSelectivasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockGarantiasSelectivasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StockGarantiasSelectivasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
