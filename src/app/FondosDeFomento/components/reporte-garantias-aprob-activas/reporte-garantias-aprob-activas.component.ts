import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reporte-garantias-aprob-activas',
  templateUrl: './reporte-garantias-aprob-activas.component.html',
  styleUrls: ['./reporte-garantias-aprob-activas.component.scss']
})
export class ReporteGarantiasAprobActivasComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
