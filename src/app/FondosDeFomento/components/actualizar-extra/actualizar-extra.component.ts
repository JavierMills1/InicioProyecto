import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-actualizar-extra',
  templateUrl: './actualizar-extra.component.html',
  styleUrls: ['./actualizar-extra.component.scss']
})
export class ActualizarExtraComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
