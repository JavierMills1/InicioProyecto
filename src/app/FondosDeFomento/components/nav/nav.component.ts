import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {

  mostarSubidaArchivos:boolean = false;
  filtroDefault:boolean = false;
  filtroPN31:boolean = false;
  filtroConciliacionMen:boolean = false;
  mostrarModal:boolean = false;
  desactivarVista:boolean = false;

  public garantiaA: string = 'Garantia Automatica';
  public garantiaS: string = 'Garantia Selectiva';
  public contratoPYME: string = 'Solicitud Contrato PYME';
  public garantiaCOMEXTMEC: string = 'Garantia Comex TMEC';
  public garantiaT: string = 'Garantia Turismo';
  public garantiaTMEC: string = 'Garantia TMEC';
  public Reestructuras: string = 'Solicitudes Reestructuras';
  //status de garantia

  public CapturarSolicitud: string = 'Capturar Solcitud';
  public ProcesoPendienteAsesorC: string = 'Proceso Pendiente Asesor Comex';
  public EspecialistaComexRech: string = 'Especialista Comex (Rechazos)';
  public ProcesoPendienteContraloria: string = 'Proceso Pendiente Contraloría';
  public FondosDeFomento: string = 'Fondos de Fomento';
  public EnviadaNafinet: string = 'Enviada a Nafinet';
  public AprobadoNafinet: string = 'Aprobado Nafinet';
  public RechaxoNafinet: string = 'Rechazo Nafinet';
  public Reproceso: string = 'Reproceso';

  fecha: Date = new Date();

  menuProcesosNafin: any = [
    { isActive: 'false', description: 'Generar Carga Nafin' },
    {
      isActive: 'false',
      description: 'Resultado Nafin',
      modal: '#ResultadoNafin',
    },
    { isActive: 'false', description: 'Folios Nafin' },
  ];

  menuSolicitudes: any = [
    { isActive: 'false', description: 'Solicitudes Automaticas' },
    { isActive: 'false', description: 'Solicitudes Selectivas' },
    { isActive: 'false', description: 'Solicitud TMEC' },
    { isActive: 'false', description: 'Solicitud Hotelero' },
    { isActive: 'false', description: 'Solicitud Contrato Pyme' },
    { isActive: 'false', description: 'Solicitudes Comex-TMEC' },
    { isActive: 'false', description: 'Solicitudes Turismo' },
  ];

  menuConciliaciones: any = [
    { isActive: 'false', description: 'Creditos PN31' },
    { isActive: 'false', description: 'Conciliacion Mensual' },
    { isActive: 'false', description: 'Creditos Sin Movs' },
    { isActive: 'false', description: 'Generar Conciliacion' },
    { isActive: 'false', description: 'Generar Reporte BMX' },
    { isActive: 'false', description: 'Conciliacion Historica' },
    { isActive: 'false', description: 'Meses Sobregiro' },
    { isActive: 'false', description: 'Movs Diarios Creditos' },
    { isActive: 'false', description: 'Movs Cliente' },
    { isActive: 'false', description: 'Movimientos Credito' },
    { isActive: 'false', description: 'Enviar a Excepcion' },
    { isActive: 'false', description: 'Conciliacion de Credito' },
    { isActive: 'false', description: 'Conciliacion Garantia' },
    { isActive: 'false', description: 'Conciliaciones Generadas' },
    { isActive: 'false', description: 'Catalogo PD97' },
    { isActive: 'false', description: 'Generar Polizas Contables' },
    { isActive: 'false', description: 'Generar Polizas Contables Sel' },
  ];

  menuControlGar: any = [
    { isActive: 'false', description: 'Creditos en Gtia' },
    { isActive: 'false', description: 'Garantia sin Creditos' },
    { isActive: 'false', description: 'Creditos sin Garantias' },
    { isActive: 'false', description: 'Reporte General Gtias' },
    { isActive: 'false', description: 'Stock Bancomex' },
    { isActive: 'false', description: 'Registrar Garantia en SIG' },
    { isActive: 'false', description: 'Creditos en Excepcion' },
    { isActive: 'false', description: 'Extra Bancomext Completo' },
    { isActive: 'false', description: 'Vigentes en Extra' },
    { isActive: 'false', description: 'Extra VS SIG' },
    { isActive: 'false', description: 'Sincronizar SIG VS Extra' },
    { isActive: 'false', description: 'Calificacion Cartera' },
    { isActive: 'false', description: 'Gtias Bmx VS Gtias Bco' },
    { isActive: 'false', description: 'Pendientes' },
  ];

  menuActualizar: any = [
    { isActive: 'false', description: 'Productos Asociados' },
    { isActive: 'false', description: 'Carga PN31' },
    { isActive: 'false', description: 'Carga PD97' },
    { isActive: 'false', description: 'Actualizar Extra' },
    { isActive: 'false', description: 'Subir Respuesta Nafin' },
    { isActive: 'false', description: 'Cargar Portafolio' },
  ];

  menuProgramas: any = [
    { isActive: 'false', description: 'Buscar Gtia de Cliente' },
    { isActive: 'false', description: 'Catalogo de Programas' },
    { isActive: 'false', description: 'Alta de Programa' },
  ];

  menuGtiasSelectivas: any = [
    { isActive: 'false', description: 'Stock Garantias Selectivas' },
    { isActive: 'false', description: 'Pendientes' },
    { isActive: 'false', description: 'Generar Conciliacion' },
    { isActive: 'false', description: 'Creditos con Garantias' },
    { isActive: 'false', description: 'Creditos en Excepcion' },
    { isActive: 'false', description: 'Conciliaciones Generadas' },
    { isActive: 'false', description: 'Selectivas' },
    { isActive: 'false', description: 'Conciliacion Mensual' },
  ];

  menuReportes: any = [
    {isActive:"false", description:"Crear Reporte Ejecutivo"},
    {isActive:"false", description:"Buscar Reporte Ejecutivo"},
    {isActive:"false", description:"Reporte Solicitudes Recibidas"},
    {isActive:"false", description:"Solicitudes Realizadas"},
    {isActive:"false", description:"Reporte Gtias Aprob/Activas"}
  ];

  allUsers: any = [];
  constructor(private http: HttpClient) {
    this.mostarVistaConciliaciones("default"),
    this.mostarVistaControlGtias("default"),
    this.mostarVistaActualizar("default"),
    this.mostarVistaProgramas("default"),
    this.mostarVistaGtiaSelectivas("default"),
    this.mostarVistaReportes("default")
  }
  esconder: boolean = false;
  esconderF: boolean = true;
  mostrarNavbar: boolean = true;

  // Automaticas
  mostrarTablaAutomaticas: boolean = false;
  mostrarTablaComextmec: boolean = false;
  mostrarTablaPyme: boolean = false;
  mostrarTablaSelectivas: boolean = false;
  mostrarTablaTmec: boolean = false;
  mostrarTablaTurismo: boolean = false;
  mostrarTablaHotelero: boolean = false;
  mostrarTablaReestructuras: boolean = false;
  mostrarSolicitudReestructuras : boolean = false;

  

  //

  mostrarSolicitudAutomatica: boolean = false;

  mostrarSolicitudSelectiva: boolean = false;

  mostrarSolicitudComexTMEC: boolean = false;

  mostrarContratoPyme: boolean = false;

  mostrarSolicitudTMEC: boolean = false;

  mostrarSolicitudTurismo: boolean = false;

  mostrarSolicitudHotelero: boolean = false;

  ngOnInit(): void {
    console.log(
      'menuConciliaciones[0].isActive: ',
      this.menuConciliaciones[0].isActive
    );
    this.esconder;
    this.mostarSubidaArchivos = false;
    this.filtroDefault = true;
    this.filtroPN31 = false;
    this.filtroConciliacionMen = false;
  }



  mostrarTablasAutomaticas() {
    this.mostrarTablaAutomaticas = true;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }
  mostrarTablasSelectivas() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = true;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }

  mostrarTablasContratoPyme() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = true;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }

  mostrarTablasComexTmec() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = true;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }

  mostrarTablasTurismo() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = true;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }

  mostrarTablasTmec() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = true;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }

  mostrarTablasHotelero() {
    this.mostrarTablaHotelero = true;
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
  }

  mostrarTablasReestructuras() {
    this.mostrarTablaHotelero = false;
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = true;
  }

  // fin tablas

  mostrarAutomatica() {
    this.mostrarSolicitudAutomatica = true;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarSelectiva() {
    this.mostrarSolicitudSelectiva = true;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarComexTMEC() {
    this.mostrarSolicitudComexTMEC = true;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }

  inicio() {
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
    this.mostrarSolicitudHotelero = false;
    this.mostrarSolicitudReestructuras = false;
  }

  mostrarPYME() {
    this.mostrarContratoPyme = true;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarTMEC() {
    this.mostrarSolicitudTMEC = true;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarTurismo() {
    this.mostrarSolicitudTurismo = true;
    this.mostrarSolicitudTMEC = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarHotelero() {
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = true;
  }
  mostrarReestructuras() {
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructuras = true;
    this.mostrarSolicitudHotelero = false;
  }
  

mostarArchivos(){

    this.mostarSubidaArchivos = true;

  }
  
  mostarVistaSolicitudes(item:string){
    for (let index = 0; index < this.menuSolicitudes.length; index++) {
      this.menuSolicitudes[index].isActive =
        this.menuSolicitudes[index].description == item ? true : false;
      console.log(
        this.menuSolicitudes[index].description +
          '-' +
          this.menuSolicitudes[index].isActive
      );
    }
  }

  mostarVistaConciliaciones(item:string){
    if (this.desactivarVista) {
      this.desactivarVista = false;
      this.mostarVistaControlGtias("desactivarModal");
    }
    this.filtroDefault = false;
    for (let index = 0; index < this.menuConciliaciones.length; index++) {      
      this.menuConciliaciones[index].isActive = this.menuConciliaciones[index].description == item ? true : false;
      if (this.menuConciliaciones[index].isActive) {
        this.desactivarVista = true;
      }
  }
}

  mostarVistaControlGtias(item:string){
    if (this.desactivarVista) {
      this.desactivarVista = false;
      this.mostarVistaConciliaciones("desactivarModal");
    }
    this.filtroDefault = false;
    for (let index = 0; index < this.menuControlGar.length; index++) {
      this.menuControlGar[index].isActive = this.menuControlGar[index].description == item ? true : false;
      if (this.menuControlGar[index].isActive) {
        this.desactivarVista = true;
      }
    }
  }


  mostarVistaActualizar(item:string){
    if (this.desactivarVista) {
      this.desactivarVista = false;
      this.mostarVistaControlGtias("desactivarModal");
      this.mostarVistaConciliaciones("desactivarModal");
    }
    this.filtroDefault = false;
    for (let index = 0; index < this.menuActualizar.length; index++) {
      this.menuActualizar[index].isActive =
        this.menuActualizar[index].description == item ? true : false;
      console.log(
        this.menuActualizar[index].description +
          '-' +
          this.menuActualizar[index].isActive
      );
    }
  }

  mostarVistaProgramas(item:string){
    this.filtroDefault = false;
    for (let index = 0; index < this.menuProgramas.length; index++) {
      this.menuProgramas[index].isActive =
        this.menuProgramas[index].description == item ? true : false;
      console.log(
        this.menuProgramas[index].description +
          '-' +
          this.menuProgramas[index].isActive
      );
    }
  }

  mostarVistaGtiaSelectivas(item:string){
    this.filtroDefault = false;
    for (let index = 0; index < this.menuGtiasSelectivas.length; index++) {
      this.menuGtiasSelectivas[index].isActive =
        this.menuGtiasSelectivas[index].description == item ? true : false;
      console.log(
        this.menuGtiasSelectivas[index].description +
          '-' +
          this.menuGtiasSelectivas[index].isActive
      );
    }
  }

  mostarVistaReportes(item:string){
    this.filtroDefault = false;
    for (let index = 0; index < this.menuReportes.length; index++) {
      this.menuReportes[index].isActive =
        this.menuReportes[index].description == item ? true : false;
      console.log(
        this.menuReportes[index].description +
          '-' +
          this.menuReportes[index].isActive
      );
    }
  }

  excepcion() {
    alert('Hola mundo');
    console.log('Hola Mundo');
  }
}
