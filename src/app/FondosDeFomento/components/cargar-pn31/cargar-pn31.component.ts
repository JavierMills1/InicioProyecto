import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cargar-pn31',
  templateUrl: './cargar-pn31.component.html',
  styleUrls: ['./cargar-pn31.component.scss']
})
export class CargarPn31Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
