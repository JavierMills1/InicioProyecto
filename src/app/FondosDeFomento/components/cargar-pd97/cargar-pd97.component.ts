import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cargar-pd97',
  templateUrl: './cargar-pd97.component.html',
  styleUrls: ['./cargar-pd97.component.scss']
})
export class CargarPd97Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
