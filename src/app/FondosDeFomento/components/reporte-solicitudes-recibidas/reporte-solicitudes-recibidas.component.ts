import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reporte-solicitudes-recibidas',
  templateUrl: './reporte-solicitudes-recibidas.component.html',
  styleUrls: ['./reporte-solicitudes-recibidas.component.scss']
})
export class ReporteSolicitudesRecibidasComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const bmxModal = document.querySelector("button");
    bmxModal?.click();
  }

}
