import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FondosdefomentoComponent } from './pages/fondosdefomento/fondosdefomento.component';
import { NavComponent } from './components/nav/nav.component';
import { TableComponent } from './components/table/table.component';
import { FormComponent } from './components/form/form.component';
import { SharedModule } from '../shared/shared.module';
import { CreditosPN31Component } from './components/creditos-pn31/creditos-pn31.component';
import { ConciliacionMensualComponent } from './components/conciliacion-mensual/conciliacion-mensual.component';
import { CreditosSinMovsComponent } from './components/creditos-sin-movs/creditos-sin-movs.component';
import { GenerarConciliacionComponent } from './components/generar-conciliacion/generar-conciliacion.component';
import { GenerarReporteBmxComponent } from './components/generar-reporte-bmx/generar-reporte-bmx.component';
import { ConciliacionHistoricaComponent } from './components/conciliacion-historica/conciliacion-historica.component';
import { MesesSobreGiroComponent } from './components/meses-sobre-giro/meses-sobre-giro.component';
import { MovsDiariosCreditosComponent } from './components/movs-diarios-creditos/movs-diarios-creditos.component';
import { MovsClienteComponent } from './components/movs-cliente/movs-cliente.component';
import { MovimientosCreditoComponent } from './components/movimientos-credito/movimientos-credito.component';
import { ConciliacionCreditoComponent } from './components/conciliacion-credito/conciliacion-credito.component';
import { EnviarAExcepcionComponent } from './components/enviar-a-excepcion/enviar-a-excepcion.component';
import { ConciliacionGarantiaComponent } from './components/conciliacion-garantia/conciliacion-garantia.component';
import { ConciliacionGeneradasComponent } from './components/conciliacion-generadas/conciliacion-generadas.component';
import { CatalogoPd97Component } from './components/catalogo-pd97/catalogo-pd97.component';
import { GenerarPolizasContablesComponent } from './components/generar-polizas-contables/generar-polizas-contables.component';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { CreditosEnGarantiaComponent } from './components/creditos-en-garantia/creditos-en-garantia.component';
import { GarantiaSinCreditoComponent } from './components/garantia-sin-credito/garantia-sin-credito.component';
import { CreditosSinGarantiaComponent } from './components/creditos-sin-garantia/creditos-sin-garantia.component';
import { ReporteGeneralGarantiasComponent } from './components/reporte-general-garantias/reporte-general-garantias.component';
import { StockBancomextComponent } from './components/stock-bancomext/stock-bancomext.component';
import { RegistrarGarantiasSigComponent } from './components/registrar-garantias-sig/registrar-garantias-sig.component';
import { CreditosEnExcepcionComponent } from './components/creditos-en-excepcion/creditos-en-excepcion.component';
import { ExtraBancomextCompletoComponent } from './components/extra-bancomext-completo/extra-bancomext-completo.component';
import { VigentesEnExtraComponent } from './components/vigentes-en-extra/vigentes-en-extra.component';
import { ExtraVsSigComponent } from './components/extra-vs-sig/extra-vs-sig.component';
import { SincronizarSigVsExtraComponent } from './components/sincronizar-sig-vs-extra/sincronizar-sig-vs-extra.component';
import { CalificacionCarteraComponent } from './components/calificacion-cartera/calificacion-cartera.component';
import { PendientesComponent } from './components/pendientes/pendientes.component';
import { ProductosAsociadosComponent } from './components/productos-asociados/productos-asociados.component';
import { CargarPn31Component } from './components/cargar-pn31/cargar-pn31.component';
import { CargarPd97Component } from './components/cargar-pd97/cargar-pd97.component';
import { ActualizarExtraComponent } from './components/actualizar-extra/actualizar-extra.component';
import { CargarPortafolioComponent } from './components/cargar-portafolio/cargar-portafolio.component';
import { BuscarGtiaClienteComponent } from './components/buscar-gtia-cliente/buscar-gtia-cliente.component';
import { CatalogoProgramasComponent } from './components/catalogo-programas/catalogo-programas.component';
import { StockGarantiasSelectivasComponent } from './components/stock-garantias-selectivas/stock-garantias-selectivas.component';
import { PendientesGtiasSelectivasComponent } from './components/pendientes-gtias-selectivas/pendientes-gtias-selectivas.component';
import { GenerarConciliacionGtiasSelectivasComponent } from './components/generar-conciliacion-gtias-selectivas/generar-conciliacion-gtias-selectivas.component';
import { CreditosConGarantiasComponent } from './components/creditos-con-garantias/creditos-con-garantias.component';
import { CreditosEnExcepcionGtiasSelectivasComponent } from './components/creditos-en-excepcion-gtias-selectivas/creditos-en-excepcion-gtias-selectivas.component';
import { ConciliacionesGeneradasSelectivasComponent } from './components/conciliaciones-generadas-selectivas/conciliaciones-generadas-selectivas.component';
import { ConciliacionMensualGtiasSelectivasComponent } from './components/conciliacion-mensual-gtias-selectivas/conciliacion-mensual-gtias-selectivas.component';
import { CrearReporteEjecutivoComponent } from './components/crear-reporte-ejecutivo/crear-reporte-ejecutivo.component';
import { BuscarReporteEjecutivoComponent } from './components/buscar-reporte-ejecutivo/buscar-reporte-ejecutivo.component';
import { ReporteSolicitudesRecibidasComponent } from './components/reporte-solicitudes-recibidas/reporte-solicitudes-recibidas.component';
import { SolicitudesRealizadasComponent } from './components/solicitudes-realizadas/solicitudes-realizadas.component';
import { ReporteGarantiasAprobActivasComponent } from './components/reporte-garantias-aprob-activas/reporte-garantias-aprob-activas.component';



@NgModule({
  declarations: [
    FondosdefomentoComponent,
    NavComponent,
    TableComponent,
    FormComponent,
    CreditosPN31Component,
    ConciliacionMensualComponent,
    CreditosSinMovsComponent,
    GenerarConciliacionComponent,
    GenerarReporteBmxComponent,
    ConciliacionHistoricaComponent,
    MesesSobreGiroComponent,
    MovsDiariosCreditosComponent,
    MovsClienteComponent,
    MovimientosCreditoComponent,
    ConciliacionCreditoComponent,
    EnviarAExcepcionComponent,
    ConciliacionGarantiaComponent,
    ConciliacionGeneradasComponent,
    CatalogoPd97Component,
    GenerarPolizasContablesComponent,
    CreditosEnGarantiaComponent,
    GarantiaSinCreditoComponent,
    CreditosSinGarantiaComponent,
    ReporteGeneralGarantiasComponent,
    StockBancomextComponent,
    RegistrarGarantiasSigComponent,
    CreditosEnExcepcionComponent,
    ExtraBancomextCompletoComponent,
    VigentesEnExtraComponent,
    ExtraVsSigComponent,
    SincronizarSigVsExtraComponent,
    CalificacionCarteraComponent,
    PendientesComponent,
    ProductosAsociadosComponent,
    CargarPn31Component,
    CargarPd97Component,
    ActualizarExtraComponent,
    CargarPortafolioComponent,
    BuscarGtiaClienteComponent,
    CatalogoProgramasComponent,
    StockGarantiasSelectivasComponent,
    PendientesGtiasSelectivasComponent,
    GenerarConciliacionGtiasSelectivasComponent,
    CreditosConGarantiasComponent,
    CreditosEnExcepcionGtiasSelectivasComponent,
    ConciliacionesGeneradasSelectivasComponent,
    ConciliacionMensualGtiasSelectivasComponent,
    CrearReporteEjecutivoComponent,
    BuscarReporteEjecutivoComponent,
    ReporteSolicitudesRecibidasComponent,
    SolicitudesRealizadasComponent,
    ReporteGarantiasAprobActivasComponent
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,   
    AsesorComexModule

  ],
  exports:[
    FondosdefomentoComponent
  ]
})
export class FondosdefomentoModule { }
