import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableAdminComponent } from './components/table-admin/table-admin.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { DataTablesModule } from 'angular-datatables';
import { NavComponent } from './components/nav/nav.component';
import { AdminComponent } from './pages/admin/admin.component';
import { FiltroAdministrarComponent } from './components/filtro-administrar/filtro-administrar.component';



@NgModule({
  declarations: [
    TableAdminComponent,
    NavComponent,
    AdminComponent,
    FiltroAdministrarComponent

  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AsesorComexModule,
    DataTablesModule
    
  ]
})
export class AdminModule { }