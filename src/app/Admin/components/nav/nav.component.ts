import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  fecha: Date = new Date();
  mostrarAdminstrar: boolean = false;


  mostrarTablaAutomaticas = false;

  
  ngOnInit(): void {

  }

  mostrarAdministrarUsuarios() {
    this.mostrarAdminstrar = true;
    this.mostrarTablaAutomaticas= false;

  }




  inicio() {
    this.mostrarAdminstrar = false;
    this.mostrarTablaAutomaticas = false;
  }


}
