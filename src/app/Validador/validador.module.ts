import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ValidadorComponent } from './pages/validador/validador.component';
import { SharedModule } from '../shared/shared.module';
import { EspecialistacomexModule } from '../EspecialistaComex/especialistacomex.module';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    NavbarComponent,
    ValidadorComponent,
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    FormsModule,
    SharedModule,
    AsesorComexModule,
    RouterModule,
  ]
})
export class ValidadorModule { }
