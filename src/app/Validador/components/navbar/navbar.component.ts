import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SteperService } from 'src/app/AsesorComex/components/steper/steper.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public garantiaHoteleto: string = 'Generar Solicitud Hotelero';

  fecha: Date = new Date();

  mostrarSolicitudAutomatica: boolean = false;
  mostrarSolicitudHotelero: boolean = false;

  constructor(private steperService: SteperService) {}

  ngOnInit(): void {}
  
  inicio() {
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudHotelero = false;
  }

  mostrarAutomatica() {
    this.mostrarSolicitudAutomatica = true;
    this.mostrarSolicitudHotelero = false;
  }

  mostrarHotelero() {
    this.mostrarSolicitudHotelero = true;
    this.mostrarSolicitudAutomatica = false;
  }

}
