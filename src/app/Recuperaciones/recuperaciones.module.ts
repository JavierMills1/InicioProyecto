import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { DataTablesModule } from 'angular-datatables';
import { NavComponent } from './components/nav/nav.component';
import { RecuperacionesComponent } from './pages/recuperaciones/recuperaciones.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    NavComponent,
    RecuperacionesComponent
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    FormsModule,
    SharedModule,
    AsesorComexModule,
    RouterModule
  ]
})
export class RecuperacionesModule { }
