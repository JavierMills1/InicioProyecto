import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  fecha: Date = new Date();
  mostrarSolicitudAutomatica: boolean = false;

  mostrarTablaAutomaticas = true;


  ngOnInit(): void {

  }

  mostrarAutomatica() {
    this.mostrarSolicitudAutomatica = true;

  }

  inicio() {
    this.mostrarSolicitudAutomatica = false;
    this.mostrarTablaAutomaticas = false;
  }

  mostrarTablasAutomaticas() {
    this.mostrarTablaAutomaticas = true;
  }
}
