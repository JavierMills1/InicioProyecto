import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ClienteBusqueda } from 'src/app/Models/ClienteBusqueda';
import { TableService } from 'src/app/AsesorComex/components/table/table.service';
import { ThisReceiver } from '@angular/compiler';

@Component({
  selector: 'app-reportes-especialista',
  templateUrl: './reportes-especialista.component.html',
  styleUrls: ['./reportes-especialista.component.scss']
})
export class ReportesEspecialistaComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  allUsers: any = [];
  allUsersComplete: any = [];

  constructor(private http: HttpClient, private tableService : TableService) { }

  esconder:boolean = false;
  esconderF:boolean = true;
  esconderT:boolean = false;
  showModal:boolean = false;
  reporte: boolean = false;

  ngOnInit(): void {

     this.getDataTable();  

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40
    };




  }
  getDataTable() {
    this.tableService.getDataTable().subscribe((resp) => {
      this.allUsers = resp;
      console.log(this.allUsers);
      this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }


ngOnDestroy(): void {
  this.dtTrigger.unsubscribe();
}

mostrar(): void{
  this.esconder = true;
  this.esconderF = false;
}
mostrarTabla(){
  this.esconderT = true;
  console.log(this.esconderT)

}




}
