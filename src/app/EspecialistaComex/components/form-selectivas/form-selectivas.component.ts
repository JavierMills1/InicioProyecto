import { Component, OnInit } from '@angular/core';
import { SteperService } from 'src/app/AsesorComex/components/steper/steper.service';

@Component({
  selector: 'app-form-selectivasEspecialista',
  templateUrl: './form-selectivas.component.html',
  styleUrls: ['./form-selectivas.component.scss']
})
export class FormSelectivasComponent implements OnInit {
  mostrarComentario:boolean = false;
  comentario:string = "";
  fechacompleta:any = "";
  alert:boolean = false;
  show: boolean = false;
  public CapturarSolicitud : string = "Capturar Solcitud"
  public ProcesoPendienteAsesorC : string = "Proceso Pendiente Asesor Comex"
  public EspecialistaComexRech : string = "Especialista Comex (Rechazos)"
  public ProcesoPendienteContraloria : string = "Proceso Pendiente Contraloría"
  public FondosDeFomento : string = "Fondos de Fomento"
  public EnviadaNafinet : string = "Enviada a Nafinet"
  public AprobadoNafinet : string = "Aprobado Nafinet"
  public RechaxoNafinet : string = "Rechazo Nafinet"
  public Reproceso : string = "Reproceso"

  fecha: Date = new Date();

  buc: string = "";
  nombreORazonSocial: string = "";
  ciudad: string = "";
  colonia: string = "";
  calleNumero: string = "";
  cp: string = "";
  delegacion: string = "";
  personalOcupado: string = "";
  sectorGeneral: string = "Seleccionar";
  sectorActivida: string = "Seleccionar";
  antiguedadAcreditado: string = "";
  propositoProyecto:string = "Seleccionar";
  porcentajeNacional: string = "";
  porcentajeMercadoInterno: string = "";
  tipoCredito:string = "Seleccionar";
  destinoRecursos:string ="Seleccionar";
  moneda:string ="Seleccionar";
  rfc="";
  porcentajeMercadoExterno="";
  date="";
  tipoTasa="";
  entidad: string = "";
  giroAcreditado: string = "";
  promedioAnual: string ="";
  ContactoRepresentante: string ="";
  telefono: string = "";
  correo: string = "";
  gobiernoVende: string = "";
  garantiaOtorgada: string ="";
  montoOtorgada: string ="";
  porcentajeParcial: number =100;
  porcentajeImportacion:string ="";
  CveFinanciamiento:string ="N/A";
  Programa: string ="";
  importeCredito : string = "";
  fuentes: string = "RECURSOS PROPIOS";
  disposicion : string = "";
  amortizacion: string ="";
  Fdisposicion: string = "N/A";
  Idisposicion: string = "N/A";
  TAutorizacion: string = "COMITE DE CREDITO";
  calificacion: string = "";
  Plazo: string = "";
  periodo: string = "";
  Periodicidad: string = "";
  Spread: string ="";
  tasaInteres: string ="";
  prioricidadInteres: string ="";
  tasaMoratoria : string ="";
  dispos1: string="N/A";
  seguros1: string ="N/A";
  comisiones: string = "";
  codigoBien: string = "";
  Tgarantia: string ="";
  etiquetaNormal: string ="";

  

  



  steper:any;


  constructor(private steperService:SteperService) {

  }

  ngOnInit(): void {

  }

  mostrarContenido() {
    this.show = true;
  }

  closeAlert(){
    this.alert = false;
  }

  MostrarAlert(){
    this.steper = this.steperService.setActive("paso4");
    this.alert = true;
  }

}
