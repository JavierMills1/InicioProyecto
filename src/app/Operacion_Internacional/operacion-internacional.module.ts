import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperacionesInternacionalesComponent } from './pages/operaciones-internacionales/operaciones-internacionales.component';
import { SharedModule } from '../shared/shared.module';
import { NavComponent } from './components/nav/nav.component';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { DataTablesModule } from 'angular-datatables';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FoliosNafinetComponent } from './components/folios-nafinet/folios-nafinet.component';
import { ReSolicitudRecibidaComponent } from './components/re-solicitud-recibida/re-solicitud-recibida.component';
import { ReGtiasAprobComponent } from './components/re-gtias-aprob/re-gtias-aprob.component';
import { BolsasEnTramiteComponent } from './components/bolsas-en-tramite/bolsas-en-tramite.component';
import { ControlGrupoCorreosComponent } from './components/control-grupo-correos/control-grupo-correos.component';
import { BitacoraCorreosComponent } from './components/bitacora-correos/bitacora-correos.component';
import { AltaUsuarioEnGrupoComponent } from './components/alta-usuario-en-grupo/alta-usuario-en-grupo.component';
import { SobreGirosComponent } from './components/sobre-giros/sobre-giros.component';
import { BitacoraEnBolsasComponent } from './components/bitacora-en-bolsas/bitacora-en-bolsas.component';
import { TableFolioNafinetComponent } from './Tables/table-folio-nafinet/table-folio-nafinet.component';
import { TableBolsasEnTramiteComponent } from './Tables/table-bolsas-en-tramite/table-bolsas-en-tramite.component';
import { TableBitacoraEnBolsaComponent } from './Tables/table-bitacora-en-bolsa/table-bitacora-en-bolsa.component';
import { DetalleBitacoraComponent } from './components/detalle-bitacora/detalle-bitacora.component';
import { GarantiasModule } from '../Garantias/garantias.module';
import { ModalAutorizarComponent } from './components/modal-autorizar/modal-autorizar.component';
import { TableGrupoCorreoComponent } from './Tables/table-grupo-correo/table-grupo-correo.component';
import { TableConsultaBitacoraCorreosComponent } from './Tables/table-consulta-bitacora-correos/table-consulta-bitacora-correos.component';
import { SobregirosComponent } from './Tables/sobregiros/sobregiros.component';



@NgModule({
  declarations: [
    OperacionesInternacionalesComponent,
    NavComponent,
    FoliosNafinetComponent,
    ReSolicitudRecibidaComponent,
    ReGtiasAprobComponent,
    BolsasEnTramiteComponent,
    ControlGrupoCorreosComponent,
    BitacoraCorreosComponent,
    AltaUsuarioEnGrupoComponent,
    SobreGirosComponent,
    BitacoraEnBolsasComponent,
    TableFolioNafinetComponent,
    TableBolsasEnTramiteComponent,
    TableBitacoraEnBolsaComponent,
    DetalleBitacoraComponent,
    ModalAutorizarComponent,
    TableGrupoCorreoComponent,
    TableConsultaBitacoraCorreosComponent,
    SobregirosComponent,
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,   
    AsesorComexModule,
    GarantiasModule

  ],
  exports:[
    FoliosNafinetComponent,
    TableFolioNafinetComponent,
    ControlGrupoCorreosComponent,
    TableGrupoCorreoComponent,
    AltaUsuarioEnGrupoComponent

  ]
})
export class OperacionInternacionalModule { }
