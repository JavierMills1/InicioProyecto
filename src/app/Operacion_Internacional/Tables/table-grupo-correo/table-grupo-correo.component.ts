import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { GrupoDeCorreos } from 'src/app/Models/TGruposDeCorreos';
import { OperacionInternacionalService } from '../../operacion-internacional.service';

@Component({
  selector: 'app-table-grupo-correo',
  templateUrl: './table-grupo-correo.component.html',
  styleUrls: ['./table-grupo-correo.component.scss']
})
export class TableGrupoCorreoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataGrupoDeCorreos: GrupoDeCorreos []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableServiceFolioNafinet: OperacionInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableServiceFolioNafinet.getDataTableGruposDeCorreo().subscribe((resp) => {
      this.dataGrupoDeCorreos = resp;
      console.log(this.dataGrupoDeCorreos);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

  send() {
    alert('Se a Eliminado el Correo exitosamente')
  }
  
  eliminar(valor: number){
        this.tableServiceFolioNafinet.eliminar(valor).subscribe((data) =>{
          this.getDataTableFolioNafinet()
        }, (error) => {
          console.log(error);
        }) 

  }
}
