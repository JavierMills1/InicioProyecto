import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { OperacionInternacionalService } from '../../operacion-internacional.service';
import { FoliosNafinet } from 'src/app/Models/TFoliosNafinet';
@Component({
  selector: 'app-table-folio-nafinet',
  templateUrl: './table-folio-nafinet.component.html',
  styleUrls: ['./table-folio-nafinet.component.scss']
})
export class TableFolioNafinetComponent implements OnInit {
 
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataFoliosNafinet: FoliosNafinet []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableServiceFolioNafinet: OperacionInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableServiceFolioNafinet.getDataTableFolioNafinet().subscribe((resp) => {
      this.dataFoliosNafinet = resp;
      console.log(this.dataFoliosNafinet);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }
}
