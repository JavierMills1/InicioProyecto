import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Sobregiros } from 'src/app/Models/TSobregiros';
import { OperacionInternacionalService } from '../../operacion-internacional.service';

@Component({
  selector: 'app-sobregiros',
  templateUrl: './sobregiros.component.html',
  styleUrls: ['./sobregiros.component.scss']
})
export class SobregirosComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataSobregiros: Sobregiros []= [];
  allUsersComplete: any = [];
  TablasBolsasEnTramite = false;

  constructor(private tableServiceFolioNafinet: OperacionInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableBitacoraBolsas();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableBitacoraBolsas() {
    this.tableServiceFolioNafinet.getDataTableSobregiros().subscribe((resp) => {
      this.dataSobregiros = resp;
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasBolsasEnTramite(){
    this.TablasBolsasEnTramite = true;
  }


}
