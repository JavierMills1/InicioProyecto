import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BolsasEnTramite } from 'src/app/Models/TBolsasEnTramite';
import { OperacionInternacionalService } from '../../operacion-internacional.service';

@Component({
  selector: 'app-table-bolsas-en-tramite',
  templateUrl: './table-bolsas-en-tramite.component.html',
  styleUrls: ['./table-bolsas-en-tramite.component.scss']
})
export class TableBolsasEnTramiteComponent implements OnInit {


  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataBolsasEnTramite: BolsasEnTramite []= [];
  allUsersComplete: any = [];
  TablasBolsasEnTramite = false;

  constructor(private tableServiceFolioNafinet: OperacionInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableBolsasEnTramite();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableBolsasEnTramite() {
    this.tableServiceFolioNafinet.getDataTableBolsaEnTramite().subscribe((resp) => {
      this.dataBolsasEnTramite = resp;
      console.log("Bolsas en tramite" + resp);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasBolsasEnTramite(){
    this.TablasBolsasEnTramite = true;
  }
}
