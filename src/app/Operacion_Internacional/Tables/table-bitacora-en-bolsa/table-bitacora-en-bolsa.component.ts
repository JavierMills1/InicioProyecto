import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BitacoraEnBolsa } from 'src/app/Models/TBitacora_Bolsa';
import { OperacionInternacionalService } from '../../operacion-internacional.service';

@Component({
  selector: 'app-table-bitacora-en-bolsa',
  templateUrl: './table-bitacora-en-bolsa.component.html',
  styleUrls: ['./table-bitacora-en-bolsa.component.scss']
})
export class TableBitacoraEnBolsaComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataBitacoraEnBolsas: BitacoraEnBolsa []= [];
  TablasBolsasEnTramite = false;

  constructor(private tableServiceFolioNafinet: OperacionInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableBitacoraBolsas();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableBitacoraBolsas() {
    this.tableServiceFolioNafinet.getDataTableBitacoraEnBolsas().subscribe((resp) => {
      this.dataBitacoraEnBolsas = resp;
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasBolsasEnTramite(){
    this.TablasBolsasEnTramite = true;
  }

}
