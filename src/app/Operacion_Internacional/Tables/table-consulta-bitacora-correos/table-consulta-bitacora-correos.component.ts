import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ConsultaBitacoraCorreos } from 'src/app/Models/TConsultaBitacoraCorreos';
import { OperacionInternacionalService } from '../../operacion-internacional.service';


@Component({
  selector: 'app-table-consulta-bitacora-correos',
  templateUrl: './table-consulta-bitacora-correos.component.html',
  styleUrls: ['./table-consulta-bitacora-correos.component.scss']
})
export class TableConsultaBitacoraCorreosComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataConsultaBitacoraCorreos: ConsultaBitacoraCorreos []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableServiceFolioNafinet: OperacionInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableServiceFolioNafinet.getDataTableConsultaBitacoraCorreos().subscribe((resp) => {
      this.dataConsultaBitacoraCorreos = resp;
      console.log(this.dataConsultaBitacoraCorreos);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

}
