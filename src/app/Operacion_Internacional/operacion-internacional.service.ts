import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FoliosNafinet } from '../Models/TFoliosNafinet';
import { HttpClient } from '@angular/common/http';
import { BolsasEnTramite } from '../Models/TBolsasEnTramite';
import { BitacoraEnBolsa } from '../Models/TBitacora_Bolsa';
import { GrupoDeCorreos } from '../Models/TGruposDeCorreos';
import { ConsultaBitacoraCorreos } from '../Models/TConsultaBitacoraCorreos';
import { Sobregiros } from '../Models/TSobregiros';

@Injectable({
  providedIn: 'root'
})
export class OperacionInternacionalService {
  public url = "https://my-json-server.typicode.com/JavierMills";

  constructor( private http: HttpClient) { }

// APIS hacia las data tables

  getDataTableFolioNafinet(): Observable<FoliosNafinet[]>{
  
    return this.http.get<FoliosNafinet[]>(`${this.url}/Hooks/Folio_Nafinet`);

  }


  getDataTableBolsaEnTramite(): Observable<BolsasEnTramite[]>{

    return this.http.get<BolsasEnTramite[]>(`${this.url}/FoodApp/Bolsas_Tramite`)
  }

  getDataTableBitacoraEnBolsas(): Observable<BitacoraEnBolsa[]>{

    return this.http.get<BitacoraEnBolsa[]>(`${this.url}/geolocalizacionAngular/Bitacora_en_Bolsas`)
  }

  getDataTableGruposDeCorreo(): Observable<GrupoDeCorreos[]>{

    return this.http.get<GrupoDeCorreos[]>(`${this.url}/ButtonRainbown/GruposDeCorreo`)
  }

  getDataTableConsultaBitacoraCorreos(): Observable<ConsultaBitacoraCorreos[]>{

    return this.http.get<ConsultaBitacoraCorreos[]>(`${this.url}/EnviarDatosPOST/ConsultaBitacoraCorreos`)
  }

  getDataTableSobregiros(): Observable<Sobregiros[]>{

    return this.http.get<Sobregiros[]>(`${this.url}/LoginAngularApp/SobreGiros`)
  }
  eliminar(valor: number){
    return this.http.delete<GrupoDeCorreos[]>(`${this.url}/ButtonRainbown/GruposDeCorreo/` + valor)

  }


}



