import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bolsas-en-tramite',
  templateUrl: './bolsas-en-tramite.component.html',
  styleUrls: ['./bolsas-en-tramite.component.scss'],
})
export class BolsasEnTramiteComponent implements OnInit {
  bolsasEnTramite: boolean = false;

  NombrePrograma: any = [
    { description: 'EMPRESAS COMEX' },
    { description: 'PYME COMEX MXP AUTOMATICA' },
    { description: 'PYME COMEX USD AUTOMATICA' },
    { description: 'GRANDES EMPRESAS COMEX' },
    { description: 'AUTOMOTRIZ MXP USD COMEX' },
    { description: 'CAPEX' },
    { description: 'COMERCIO EXTERIOR PYME T-MEC EMP' },
    { description: 'COMERCIO EXTERIOR PYME T-MEC ' },
    { description: 'COMERCIO EXTERIOR GRANDES EMPRESAS T-MEC' },
    { description: 'REACTIVACION ECONOMICA SECTOR HOTELERO' },
  ];

  constructor() {}

  ngOnInit(): void {}

  
  mostarBolsasTramite() {
    this.bolsasEnTramite = true;
  }
}
