import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-folios-nafinet',
  templateUrl: './folios-nafinet.component.html',
  styleUrls: ['./folios-nafinet.component.scss']
})
export class FoliosNafinetComponent implements OnInit {

  tableFolioNafinet = false;
  constructor() { }

  ngOnInit(): void {
  }

  mostraFolioNafinet(){
    this.tableFolioNafinet = true;
  }
}
