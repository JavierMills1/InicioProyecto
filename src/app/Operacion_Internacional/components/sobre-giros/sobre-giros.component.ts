import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sobre-giros',
  templateUrl: './sobre-giros.component.html',
  styleUrls: ['./sobre-giros.component.scss']
})
export class SobreGirosComponent implements OnInit {


  sobregiros: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  mostrarSobregiros(){
    this.sobregiros = true;
  }
}
