import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public garantiaA: string = 'Garantia Automatica';
  public garantiaS: string = 'Garantia Selectiva';
  public contratoPYME: string = 'Solicitud Contrato PYME';
  public garantiaCOMEXTMEC: string = 'Garantia Comex TMEC';
  public garantiaT: string = 'Garantia Turismo';
  public garantiaTMEC: string = 'Garantia TMEC';

  //status de garantia

  public CapturarSolicitud: string = 'Capturar Solcitud';
  public ProcesoPendienteAsesorC: string = 'Proceso Pendiente Asesor Comex';
  public EspecialistaComexRech: string = 'Especialista Comex (Rechazos)';
  public ProcesoPendienteContraloria: string = 'Proceso Pendiente Contraloría';
  public ProcesoPendienteCartera: string = 'Proceso Pendiente Cartera';
  public FondosDeFomento: string = 'Fondos de Fomento';
  public EnviadaNafinet: string = 'Enviada a Nafinet';
  public AprobadoNafinet: string = 'Aprobado Nafinet';
  public RechaxoNafinet: string = 'Rechazo Nafinet';
  public Reproceso: string = 'Reproceso';

  fecha: Date = new Date();

  public Automaticas: string = 'Solicitudes Automaticas';
  public Selectivas: string = 'Solicitudes Selectivas';
  public TMEC: string = 'Solicitud TMEC';
  public Contrato_PYME: string = 'Solicitud Contrato Pyme';
  public Comex_TMEC: string = 'Solicitudes Comex-TMEC';
  public Turismo: string = 'Solicitudes Turismo';
  public Reestructuras: string = 'Solicitud Reestructuras';
  public hora: string = '';

  allUsers: any = [];
  constructor(private http: HttpClient) {}
  

  // Automaticas
  mostrarTablaAutomaticas: boolean = false;
  mostrarTablaComextmec: boolean = false;
  mostrarTablaPyme: boolean = false;
  mostrarTablaSelectivas: boolean = false;
  mostrarTablaTmec: boolean = false;
  mostrarTablaTurismo: boolean = false;
  mostrarTablaReestructura : boolean = false;
  mostrarTablaReestructuras: boolean = false;
  mostrarTablaHotelero : boolean = false;

  //


  mostrarSolicitudAutomatica: boolean = false;

  mostrarSolicitudSelectiva: boolean = false;

  mostrarSolicitudComexTMEC: boolean = false;

  mostrarContratoPyme: boolean = false;

  mostrarSolicitudTMEC: boolean = false;

  mostrarSolicitudTurismo: boolean = false;

  mostrarSolicitudReestructura : boolean = false;


  mostrarFoliosNafinet: boolean = false;
  mostrarTramite: boolean = false;
  mostrarBitacoraBolsas: boolean = false;
  mostrarGruposCorreos: boolean = false;
  bitacoraCorreos: boolean = false;
  sobreGiros:boolean = false;

  ngOnInit(): void {
   
  }
  mostrarTablasComexTmec() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = true;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }

  mostrarTablasTurismo() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = true;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }


  mostrarTablasTmec() {
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = true;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = false;
    this.mostrarTablaHotelero = false;
  }


  mostrarTablasReestructuras() {
    this.mostrarTablaHotelero = false;
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarTablaReestructuras = true;
  }
// solicitudes

  mostrarComexTMEC() {
    this.mostrarSolicitudComexTMEC = true;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructura= false;

  }

  inicio() {
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarTablaAutomaticas = false;
    this.mostrarTablaComextmec = false;
    this.mostrarTablaPyme = false;
    this.mostrarTablaSelectivas = false;
    this.mostrarTablaTmec = false;
    this.mostrarTablaTurismo = false;
    this.mostrarSolicitudReestructura= false;
    this.mostrarTablaReestructura = false;
    this.mostrarFoliosNafinet = false;
    this.mostrarTramite = false;
  this.mostrarGruposCorreos = false;
  this.bitacoraCorreos = false;
  this.sobreGiros = false;


  }


  mostrarTMEC() {
    this.mostrarSolicitudTMEC = true;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudReestructura= false;

  }
  mostrarTurismo() {
    this.mostrarSolicitudTurismo = true;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructura= false;

  }
  mostrarReestructuras() {
    this.mostrarSolicitudReestructura= true;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudComexTMEC = false;
  }

mostrarSolicitudFoliosNafinet(){
  this.mostrarFoliosNafinet = true;
  this.mostrarSolicitudComexTMEC = false;
  this.mostrarSolicitudTurismo = false;
  this.mostrarSolicitudTMEC = false;
  this.mostrarSolicitudReestructura= false;
  this.mostrarSolicitudAutomatica= false;
  this.mostrarTramite = false;
  this.mostrarBitacoraBolsas = false;
  this.mostrarGruposCorreos = false;
  this.bitacoraCorreos = false;
  this.sobreGiros = false;

}

mostrarBolsasEnTramite(){
  this.mostrarFoliosNafinet = false;
  this.mostrarSolicitudComexTMEC = false;
  this.mostrarSolicitudTurismo = false;
  this.mostrarSolicitudTMEC = false;
  this.mostrarSolicitudReestructura= false;
  this.mostrarSolicitudAutomatica= false;
  this.mostrarTramite = true;
  this.mostrarBitacoraBolsas = false;
  this.mostrarGruposCorreos = false;
  this.bitacoraCorreos = false;
  this.sobreGiros = false;

}

mostrarBitacoraEnBolsas(){
  this.mostrarFoliosNafinet = false;
  this.mostrarSolicitudComexTMEC = false;
  this.mostrarSolicitudTurismo = false;
  this.mostrarSolicitudTMEC = false;
  this.mostrarSolicitudReestructura= false;
  this.mostrarSolicitudAutomatica= false;
  this.mostrarTramite = false;
  this.mostrarBitacoraBolsas = true;
  this.mostrarGruposCorreos = false;
  this.bitacoraCorreos = false;
  this.sobreGiros = false;

}


mostrarControlDeGruposDeCorreos(){
  this.mostrarFoliosNafinet = false;
  this.mostrarSolicitudComexTMEC = false;
  this.mostrarSolicitudTurismo = false;
  this.mostrarSolicitudTMEC = false;
  this.mostrarSolicitudReestructura= false;
  this.mostrarSolicitudAutomatica= false;
  this.mostrarTramite = false;
  this.mostrarBitacoraBolsas = false;
  this.mostrarGruposCorreos = true;
  this.bitacoraCorreos = false;
  this.sobreGiros = false;


}

mostrarControlBitacoraCorreos(){
  this.mostrarFoliosNafinet = false;
  this.mostrarSolicitudComexTMEC = false;
  this.mostrarSolicitudTurismo = false;
  this.mostrarSolicitudTMEC = false;
  this.mostrarSolicitudReestructura= false;
  this.mostrarSolicitudAutomatica= false;
  this.mostrarTramite = false;
  this.mostrarBitacoraBolsas = false;
  this.mostrarGruposCorreos = false;
  this.bitacoraCorreos = true;
  this.sobreGiros = false;

}

mostrarSobreGiros(){
  this.mostrarFoliosNafinet = false;
  this.mostrarSolicitudComexTMEC = false;
  this.mostrarSolicitudTurismo = false;
  this.mostrarSolicitudTMEC = false;
  this.mostrarSolicitudReestructura= false;
  this.mostrarSolicitudAutomatica= false;
  this.mostrarTramite = false;
  this.mostrarBitacoraBolsas = false;
  this.mostrarGruposCorreos = false;
  this.bitacoraCorreos = false;
  this.sobreGiros = true;
}


}



