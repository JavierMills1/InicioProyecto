import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bitacora-en-bolsas',
  templateUrl: './bitacora-en-bolsas.component.html',
  styleUrls: ['./bitacora-en-bolsas.component.scss']
})
export class BitacoraEnBolsasComponent implements OnInit {


  tableBitacoraEnBolsa : boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  mostrarBitacoraEnBolsa(){
    this.tableBitacoraEnBolsa = true;
  }

}
