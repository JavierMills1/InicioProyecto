import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-control-grupo-correos',
  templateUrl: './control-grupo-correos.component.html',
  styleUrls: ['./control-grupo-correos.component.scss']
})
export class ControlGrupoCorreosComponent implements OnInit {


  grupoDeCorreos:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  mostarGrupoCorreos(){
    this.grupoDeCorreos = true;
  }

}
