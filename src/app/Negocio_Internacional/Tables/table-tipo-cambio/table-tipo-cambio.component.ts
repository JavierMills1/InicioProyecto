import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { TipoCambio } from 'src/app/Models/TTipoCambio';
import { NegocioInternacionalService } from '../../negocio-internacional.service';

@Component({
  selector: 'app-table-tipo-cambio',
  templateUrl: './table-tipo-cambio.component.html',
  styleUrls: ['./table-tipo-cambio.component.scss']
})
export class TableTipoCambioComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataTipoCambio: TipoCambio []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableTipoCambio().subscribe((resp) => {
      this.dataTipoCambio = resp;
      console.log(this.dataTipoCambio);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  
}
