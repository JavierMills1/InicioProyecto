import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalDetalleDeBolsa } from 'src/app/Models/TModalDetalleDeBolsa';
import { NegocioInternacionalService } from '../../negocio-internacional.service';
// NavBarResponsive/Detalle_Bolsa_Modal
@Component({
  selector: 'app-table-modal-detalle-bolsa',
  templateUrl: './table-modal-detalle-bolsa.component.html',
  styleUrls: ['./table-modal-detalle-bolsa.component.scss']
})
export class TableModalDetalleBolsaComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataModalDetalleBolsa: ModalDetalleDeBolsa []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableModalDetalle().subscribe((resp) => {
      this.dataModalDetalleBolsa = resp;
      console.log(this.dataModalDetalleBolsa);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

}
