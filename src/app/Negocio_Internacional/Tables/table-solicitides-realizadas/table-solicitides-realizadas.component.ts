import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { SolicitudesRealizadas } from 'src/app/Models/TSolicitudesRealizadas';
import { NegocioInternacionalService } from '../../negocio-internacional.service';

@Component({
  selector: 'app-table-solicitides-realizadas',
  templateUrl: './table-solicitides-realizadas.component.html',
  styleUrls: ['./table-solicitides-realizadas.component.scss']
})
export class TableSolicitidesRealizadasComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataSolicitudesRealizadas: SolicitudesRealizadas []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableSolicitudesRealizadas().subscribe((resp) => {
      this.dataSolicitudesRealizadas = resp;
      console.log(this.dataSolicitudesRealizadas);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }
}
