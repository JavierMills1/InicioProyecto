import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BuscarReporte } from 'src/app/Models/buscarReporte';
import { NegocioInternacionalService } from '../../negocio-internacional.service';

@Component({
  selector: 'app-table-buscar-reporte',
  templateUrl: './table-buscar-reporte.component.html',
  styleUrls: ['./table-buscar-reporte.component.scss']
})
export class TableBuscarReporteComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataBuscarReporte: BuscarReporte []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableBuscarReporte().subscribe((resp) => {
      this.dataBuscarReporte = resp;
      console.log(this.dataBuscarReporte);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

}
