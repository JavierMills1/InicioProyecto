import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { StockSelectivas } from 'src/app/Models/TStockSelectivas';
import { NegocioInternacionalService } from '../../negocio-internacional.service';

@Component({
  selector: 'app-table-stock-selectivas',
  templateUrl: './table-stock-selectivas.component.html',
  styleUrls: ['./table-stock-selectivas.component.scss']
})
export class TableStockSelectivasComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataStockSelectivas: StockSelectivas []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableStockSelectivas().subscribe((resp) => {
      this.dataStockSelectivas = resp;
      console.log(this.dataStockSelectivas);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

  desactivar(){
    if (confirm('Desea desactivar la Garantía')) {
     
      } else {
      alert('Se Cancelo el proceso');
      }


  }

  reactivar(){
    if (confirm('Desea Reactivar la Garantía')) {
    
      } else {
      alert('Se Cancelo el proceso');
      }
  }
}
