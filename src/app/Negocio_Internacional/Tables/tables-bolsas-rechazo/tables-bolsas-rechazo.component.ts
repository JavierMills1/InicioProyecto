import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BolsasEnRechazo } from 'src/app/Models/TBolsasEnRechazo';
import { NegocioInternacionalService } from '../../negocio-internacional.service';

@Component({
  selector: 'app-tables-bolsas-rechazo',
  templateUrl: './tables-bolsas-rechazo.component.html',
  styleUrls: ['./tables-bolsas-rechazo.component.scss']
})
export class TablesBolsasRechazoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataBolsasEnRechazo: BolsasEnRechazo []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableBitacoraEnRechazo().subscribe((resp) => {
      this.dataBolsasEnRechazo = resp;
      console.log(this.dataBolsasEnRechazo);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

}
