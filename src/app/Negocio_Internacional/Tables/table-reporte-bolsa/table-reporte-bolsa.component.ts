import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ReporteBolsas } from 'src/app/Models/TReporteBolsas';
import { NegocioInternacionalService } from '../../negocio-internacional.service';

@Component({
  selector: 'app-table-reporte-bolsa',
  templateUrl: './table-reporte-bolsa.component.html',
  styleUrls: ['./table-reporte-bolsa.component.scss']
})
export class TableReporteBolsaComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  dataReporteBolsas: ReporteBolsas []= [];
  allUsersComplete: any = [];
  TablasFolioNafinet = false;

  constructor(private tableBolsasEnRechazo: NegocioInternacionalService) {}

  ngOnInit(): void {
    this.getDataTableFolioNafinet();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 40,
    };
  }

  getDataTableFolioNafinet() {
    this.tableBolsasEnRechazo.getTableReporteBolsas().subscribe((resp) => {
      this.dataReporteBolsas = resp;
      console.log(this.dataReporteBolsas);
      // this.allUsersComplete = resp;
      this.dtTrigger.next(0);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  mostrarTablasFolioNafinet(){
    this.TablasFolioNafinet = true;
  }

}
