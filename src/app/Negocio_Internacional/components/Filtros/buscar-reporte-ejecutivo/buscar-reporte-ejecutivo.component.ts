import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buscar-reporte-ejecutivo',
  templateUrl: './buscar-reporte-ejecutivo.component.html',
  styleUrls: ['./buscar-reporte-ejecutivo.component.scss']
})
export class BuscarReporteEjecutivoComponent implements OnInit {


  reporteEjecutivo: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  mostarReporteEjecutivo(){
    this.reporteEjecutivo = true;
  }
}
