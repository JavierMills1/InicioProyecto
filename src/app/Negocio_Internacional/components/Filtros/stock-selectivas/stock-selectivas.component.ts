import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stock-selectivas',
  templateUrl: './stock-selectivas.component.html',
  styleUrls: ['./stock-selectivas.component.scss']
})
export class StockSelectivasComponent implements OnInit {

  stockSelectivas: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  mostrarStockSellectivas(){
    this.stockSelectivas= true;
  }

}
