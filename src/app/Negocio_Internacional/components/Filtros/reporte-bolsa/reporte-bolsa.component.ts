import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reporte-bolsa',
  templateUrl: './reporte-bolsa.component.html',
  styleUrls: ['./reporte-bolsa.component.scss']
})
export class ReporteBolsaComponent implements OnInit {

  reporteBolsas:boolean= false;
  constructor() { }

  ngOnInit(): void {
  }
  mostrarReporteBolsas(){
    this.reporteBolsas= true;
  }

}
