import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bolsas-en-rechazo',
  templateUrl: './bolsas-en-rechazo.component.html',
  styleUrls: ['./bolsas-en-rechazo.component.scss']
})
export class BolsasEnRechazoComponent implements OnInit {
  tableBolsasRechazo: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

mostrarBolsasEnRechazo(){
  this.tableBolsasRechazo = true;
}
}
