import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-de-cambio',
  templateUrl: './tipo-de-cambio.component.html',
  styleUrls: ['./tipo-de-cambio.component.scss']
})
export class TipoDeCambioComponent implements OnInit {

  tipoCambio: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  mostrarTipoCambio(){
    this.tipoCambio= true;
  }
}
