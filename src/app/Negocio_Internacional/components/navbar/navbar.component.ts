import { Component, OnInit } from '@angular/core';
import { ShowCase } from '../../../../app/Models/ShowCase';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  mostrarBolsasEnRechazo = false;
  solicitudesRealizadas: boolean = false;
  reporteBolsa: boolean = false;
  reporteEjecutivo: boolean = false;
  folioNafinet: boolean = false;
  abrir: boolean = false;
  stockSelectivas: boolean = false;
  grupoCorreo: boolean = false;
  altaUsuario: boolean = false;
  tipoCambio: boolean = false;

  public Automaticas: string = 'Solicitudes Automaticas';
  public Selectivas: string = 'Solicitudes Selectivas';
  public TMEC: string = 'Solicitud TMEC';
  public Contrato_PYME: string = 'Solicitud Contrato Pyme';
  public Comex_TMEC: string = 'Solicitudes Comex-TMEC';
  public Turismo: string = 'Solicitudes Turismo';
  public Rees: string = 'Solicitudes Reestructuras';
  public Hotel: string = 'Solicitudes Hotelero';

  mostrarSolicitudAutomatica: boolean = false;

  mostrarSolicitudSelectiva: boolean = false;

  mostrarSolicitudComexTMEC: boolean = false;

  mostrarContratoPyme: boolean = false;

  mostrarSolicitudTMEC: boolean = false;

  mostrarSolicitudTurismo: boolean = false;

  mostrarSolicitudHotelero: boolean = false;

  mostrarSolicitudReestructuras: boolean = false;

  fecha: Date = new Date();

  constructor() {}
  mostrarAutomatica() {
    this.mostrarSolicitudAutomatica = true;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarSelectiva() {
    this.mostrarSolicitudSelectiva = true;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
    this.mostrarBolsasEnRechazo = false;
  }
  mostrarComexTMEC() {
    this.mostrarSolicitudComexTMEC = true;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }

  inicio() {
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudHotelero = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarBolsasEnRechazo = false;
    this.solicitudesRealizadas = false;
    this.reporteBolsa = false;
    this.reporteEjecutivo = false;
    this.folioNafinet = false;
    this.stockSelectivas = false;
    this.grupoCorreo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;


  }

  mostrarPYME() {
    this.mostrarContratoPyme = true;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarTMEC() {
    this.mostrarSolicitudTMEC = true;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarTurismo() {
    this.mostrarSolicitudTurismo = true;
    this.mostrarSolicitudTMEC = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = false;
  }
  mostrarHotelero() {
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructuras = false;
    this.mostrarSolicitudHotelero = true;
  }
  mostrarReestructuras() {
    this.mostrarSolicitudTurismo = false;
    this.mostrarSolicitudTMEC = false;
    this.mostrarContratoPyme = false;
    this.mostrarSolicitudAutomatica = false;
    this.mostrarSolicitudSelectiva = false;
    this.mostrarSolicitudComexTMEC = false;
    this.mostrarSolicitudReestructuras = true;
    this.mostrarSolicitudHotelero = false;
    
  }


  ngOnInit(): void {}

  bolsasEnRechazo() {
    this.mostrarBolsasEnRechazo = true;
    this.solicitudesRealizadas = false;
    this.reporteBolsa = false;
    this.reporteEjecutivo = false;
    this.folioNafinet = false;
    this.stockSelectivas = false;
    this.grupoCorreo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;

  }

  mostrarSolicitudesRealizadas() {
    this.solicitudesRealizadas = true;
    this.mostrarBolsasEnRechazo = false;
    this.reporteBolsa = false;
    this.reporteEjecutivo = false;
    this.folioNafinet = false;
    this.stockSelectivas = false;
    this.altaUsuario = false;
    this.grupoCorreo = false;
    this.tipoCambio= false;

  }
  mostrarReporteBolsa() {
    this.reporteBolsa = true;
    this.solicitudesRealizadas = false;
    this.mostrarBolsasEnRechazo = false;
    this.reporteEjecutivo = false;
    this.folioNafinet = false;
    this.stockSelectivas = false;
    this.grupoCorreo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;

  }
  mostrarBuscarReporteEjecutivo() {
    this.reporteEjecutivo = true;
    this.folioNafinet = false;
    this.reporteBolsa = false;
    this.solicitudesRealizadas = false;
    this.mostrarBolsasEnRechazo = false;
    this.stockSelectivas = false;
    this.grupoCorreo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;

  }

  mostrarFolioNafinet() {
    this.folioNafinet = true;
    this.reporteEjecutivo = false;
    this.reporteBolsa = false;
    this.solicitudesRealizadas = false;
    this.mostrarBolsasEnRechazo = false;
    this.stockSelectivas = false;
    this.grupoCorreo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;


  }

  abrirParametro() {
    this.abrir = true;
  }
  mostrarStockSelectivas() {
    this.stockSelectivas = true;
    this.folioNafinet = false;
    this.reporteEjecutivo = false;
    this.reporteBolsa = false;
    this.solicitudesRealizadas = false;
    this.mostrarBolsasEnRechazo = false;
    this.grupoCorreo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;

  }
  mostarGrupoCorreo() {
    this.grupoCorreo = true;
    this.stockSelectivas = false;
    this.folioNafinet = false;
    this.reporteEjecutivo = false;
    this.reporteBolsa = false;
    this.solicitudesRealizadas = false;
    this.mostrarBolsasEnRechazo = false;
    this.altaUsuario = false;
    this.tipoCambio= false;

  }
  mostarTipoCambio() {
    this.tipoCambio= true;
    this.grupoCorreo = false;
    this.stockSelectivas = false;
    this.folioNafinet = false;
    this.reporteEjecutivo = false;
    this.reporteBolsa = false;
    this.solicitudesRealizadas = false;
    this.mostrarBolsasEnRechazo = false;
    this.altaUsuario = false;
  }

}
