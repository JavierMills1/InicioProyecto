import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CerrarParametroComponent } from './cerrar-parametro.component';

describe('CerrarParametroComponent', () => {
  let component: CerrarParametroComponent;
  let fixture: ComponentFixture<CerrarParametroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CerrarParametroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CerrarParametroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
