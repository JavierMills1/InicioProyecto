import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BuscarReporte } from '../Models/buscarReporte';
import { BolsasEnRechazo } from '../Models/TBolsasEnRechazo';
import { ModalDetalleDeBolsa } from '../Models/TModalDetalleDeBolsa';
import { ReporteBolsas } from '../Models/TReporteBolsas';
import { SolicitudesRealizadas } from '../Models/TSolicitudesRealizadas';
import { StockSelectivas } from '../Models/TStockSelectivas';
import { TipoCambio } from '../Models/TTipoCambio';

@Injectable({
  providedIn: 'root'
})
export class NegocioInternacionalService {

  public url = "https://my-json-server.typicode.com/JavierMills";

  constructor( private http: HttpClient) { }

// APIS hacia las data tables

  getTableBitacoraEnRechazo(): Observable<BolsasEnRechazo[]>{
    return this.http.get<BolsasEnRechazo[]>(`${this.url}/CardConSass/Bolsas_En_Rechazo`)
  }

getTableSolicitudesRealizadas(): Observable<SolicitudesRealizadas[]>{
  return this.http.get<SolicitudesRealizadas[]>(`${this.url}/TypescriptBasics/Solicitudes_Realizadas`)
}

getTableReporteBolsas(): Observable<ReporteBolsas[]>{
  return this.http.get<ReporteBolsas[]>(`${this.url}/Documents/Reporte_Bolsa`)
}

getTableModalDetalle(): Observable<ModalDetalleDeBolsa[]>{
  return this.http.get<ModalDetalleDeBolsa[]>(`${this.url}/NavBarResponsive/Detalle_Bolsa_Modal`)
}

getTableBuscarReporte(): Observable<BuscarReporte[]>{
  return this.http.get<BuscarReporte[]>(`${this.url}/Jquery/Buscar_Reporte`)
}


getTableStockSelectivas(): Observable<StockSelectivas[]>{
  return this.http.get<StockSelectivas[]>(`${this.url}/GoogleClone/Stock_Selectivas`)
}

getTableTipoCambio(): Observable<TipoCambio[]>{
  return this.http.get<TipoCambio[]>(`${this.url}/NetflixCloneApp/tipoCambio`)
}
}
