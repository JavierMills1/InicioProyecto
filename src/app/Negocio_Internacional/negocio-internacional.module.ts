import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NegocioInternacionalComponent } from './pages/negocio-internacional/negocio-internacional.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SharedModule } from '../shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { ReactivarbolsaComponent } from './components/reactivarbolsa/reactivarbolsa.component';
import { GarantiasModule } from '../Garantias/garantias.module';
import { BolsasEnRechazoComponent } from './components/Filtros/bolsas-en-rechazo/bolsas-en-rechazo.component';
import { TablesBolsasRechazoComponent } from './Tables/tables-bolsas-rechazo/tables-bolsas-rechazo.component';
import { TableSolicitidesRealizadasComponent } from './Tables/table-solicitides-realizadas/table-solicitides-realizadas.component';
import { SolicitudesRealizadasComponent } from './components/Filtros/solicitudes-realizadas/solicitudes-realizadas.component';
import { ReporteBolsaComponent } from './components/Filtros/reporte-bolsa/reporte-bolsa.component';
import { ReporteEjecutivoComponent } from './components/reporte-ejecutivo/reporte-ejecutivo.component';
import { BuscarReporteEjecutivoComponent } from './components/Filtros/buscar-reporte-ejecutivo/buscar-reporte-ejecutivo.component';
import { TableReporteBolsaComponent } from './Tables/table-reporte-bolsa/table-reporte-bolsa.component';
import { ModalDetalleDeLaBolsaComponent } from './components/modal-detalle-de-la-bolsa/modal-detalle-de-la-bolsa.component';
import { TableModalDetalleBolsaComponent } from './Tables/table-modal-detalle-bolsa/table-modal-detalle-bolsa.component';
import { TableBuscarReporteComponent } from './Tables/table-buscar-reporte/table-buscar-reporte.component';
import { FoliosNafinetComponent } from '../Operacion_Internacional/components/folios-nafinet/folios-nafinet.component';
import { OperacionInternacionalModule } from '../Operacion_Internacional/operacion-internacional.module';
import { AbrirCerrarParametroComponent } from './components/abrir-cerrar-parametro/abrir-cerrar-parametro.component';
import { CerrarParametroComponent } from './components/cerrar-parametro/cerrar-parametro.component';
import { StockSelectivasComponent } from './components/Filtros/stock-selectivas/stock-selectivas.component';
import { TipoDeCambioComponent } from './components/Filtros/tipo-de-cambio/tipo-de-cambio.component';
import { TableStockSelectivasComponent } from './Tables/table-stock-selectivas/table-stock-selectivas.component';
import { TableTipoCambioComponent } from './Tables/table-tipo-cambio/table-tipo-cambio.component';
import { ModalStockSelectivasComponent } from './components/modal-stock-selectivas/modal-stock-selectivas.component';
import { ModalModificarTipoCambioComponent } from './components/modal-modificar-tipo-cambio/modal-modificar-tipo-cambio.component';
import { ModalEliminarComponent } from './components/modal-eliminar/modal-eliminar.component';


@NgModule({
  declarations: [
    NegocioInternacionalComponent,
    NavbarComponent,
    ReactivarbolsaComponent,
    BolsasEnRechazoComponent,
    TablesBolsasRechazoComponent,
    TableSolicitidesRealizadasComponent,
    SolicitudesRealizadasComponent,
    ReporteBolsaComponent,
    ReporteEjecutivoComponent,
    BuscarReporteEjecutivoComponent,
    TableReporteBolsaComponent,
    ModalDetalleDeLaBolsaComponent,
    TableModalDetalleBolsaComponent,
    TableBuscarReporteComponent,
    AbrirCerrarParametroComponent,
    CerrarParametroComponent,
    StockSelectivasComponent,
    TipoDeCambioComponent,
    TableStockSelectivasComponent,
    TableTipoCambioComponent,
    ModalStockSelectivasComponent,
    ModalModificarTipoCambioComponent,
    ModalEliminarComponent,

  ],
  imports: [
    CommonModule,
    DataTablesModule,
    HttpClientModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    AsesorComexModule,
    GarantiasModule,
    OperacionInternacionalModule
  ],

})
export class NegocioInternacionalModule { }
