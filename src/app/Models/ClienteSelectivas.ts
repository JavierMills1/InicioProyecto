export interface ClienteBusquedaSelectivas {
    idFolio:                number;
    status:                 string;
    Icon:                   string;
    Estatus:                string;
    Buc:                    string;
    Acreditado:             string;
    Solicitado:             string;
    Solicito:               string;
    A:                      string;
    Z:                      string;
    C:                        Date;
    E:                        Date;
    Archivo:                string;
    G:                      string;
    SLAS:                   string;
 
  }
  