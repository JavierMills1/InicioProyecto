export interface StockSelectivas{
    Est:                        string,
    Garantia:                   string,
    Buc:                        number,
    Razon_Social:               string,
    Moneda:                     string,
    Fecha_Alta:                 string,
    Importe_MXN:                number,
    Importe_USD:                number,
    Prog:                       number,
    Porciento_Cober:            number,
    Com_Bcmx:                   number,
    Acciones:                   string
}