export interface ClienteBusqueda {
  elim:                   string;
  status:                 string;
  id:                     number;
  R:                      string;
  I:                      string;
  Folio:                  number;
  Firma_Contrato:         number;
  Cvel_Nafinet:           number;
  Estatus:                string;
  Buc:                    string;
  Acreditado:             Date;
  Solicitado:             Date;
  Dias:                   Date;
  Solicito:               string;
  A:                      string;
  Z:                      string;
  C:                      string;
}
