export interface ModalDetalleDeBolsa{
    Folio:              string,
    Monto_Linea:        number,
    Monto_Linea_Val:    number,
    Moneda:             number,
    Sgto_Espec:         string,
    Fecha_Firm_CTRT:    string,
    Estatus_Folio:      string,
    Description:        string
}