export interface ShowCase {
    isActive:     string;
    description:  string;
  }
  