export interface CreditoGarantia{
    id:                         number,
    Fecha_Relacion:            string,
    Conc:                       number,
    Credito:                    number,
    Garantia:                   string,
    Buc:                        string,
    Razon_Social:               string,
    Moneda_Gar:                 string,
    Valor_Gatia:                string,
    Fecha_Gtia:                 string,
    Subprod:                    string,
    Moneda_Cred:                string,
    Apertura_Credito:           string,
    Sit:                        string,
    Concedido:                  number,
    Insoluto:                   number,
    Porciento_Gar:              string,
    Etiq:                       string,
    Acciones_sobre_credito:     string          

}