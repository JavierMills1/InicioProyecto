export interface BolsasEnRechazo{
    id:                 number,
    Solicitante:        string,
    Bolsa_Anterior:     number,
    Nombre_Programa:     number,
    Nueva_Bolsa:        number,
    Monto:              number,
    Gtia_MXP:           number,
    Gtia_USD:           number,
    Estatus:            string,
    Fecha_Solicitud:    string,
    Fecha_Vencimiento:  string,
    Observacion:        string,
    Adjuntos_Rechazo:   string,
    Corregir:           string
}