export interface CatalogoProgramas{
    Programa:                   number,
    Etiqueta:                   string,
    Gar_390:                    number,
    Buc_390:                    number,
    Nombre_Nafin:               string,
    Modificar:                  string
}