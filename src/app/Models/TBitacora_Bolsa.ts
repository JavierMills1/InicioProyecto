export interface BitacoraEnBolsa{
    Folio_Solicitud:                number,
    Solicito:                       string,
    Estatus:                        string,
    Bolsa_Anterior:                 number,
    Nueva_Bolsa:                    number,
    Monto_Nueva_Bolsa:              number,
    Programa:                       string,
    Etiqueta_Asignada:              number,
    Moneda:                         string,
    Garantia_ALTAIR_Pesos:          number,
    Garantia_ALTAIR_Dolares:        number,
    Fecha_Solicitud:                string,
    Detalle:                        string
}