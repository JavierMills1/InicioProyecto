export interface BuscarReporte{
    Usuario:                    string,
    Nombre_Archivo:             string,
    Periodo_Conciliacion:       string,
    Fecha_De_Creacion:          string,
    Tipo_De_Cambio_Utilizado:   number,
    Descargar:                  string,
}