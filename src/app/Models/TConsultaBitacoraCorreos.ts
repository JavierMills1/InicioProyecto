export interface ConsultaBitacoraCorreos{
    id:                         number,
    Fecha:                      string,
    Usuario:                    string,
    Expediente:                 string,
    Accion:                     string,
    Descripcion:                string
}