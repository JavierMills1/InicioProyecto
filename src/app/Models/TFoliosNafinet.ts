export interface FoliosNafinet {
    id:             number,
    Clave:          number,
    FechaCarga:     string,
    Registros:      number,
    Sumatoria:      number,
    Dias:           number,
    Usuario_Cargo:  string,
    Excel:          string,
    TXT:            string,
    PDF:            string,      
}