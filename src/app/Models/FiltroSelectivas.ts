export interface FiltroS {
    buc:      string;
    cliente:  string;
    banca:    string;
    estatus:  string;
    sucursal: string;
    region:   string;
    zona:     string;
  }
  