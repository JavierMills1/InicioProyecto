export interface GrupoDeCorreos{
    id:                     number,
    Nombre_Grupo:           string,
    Region:                 string,
    Nombre:                 string,
    Correo:                 string,
    Expediente:             string,
    Estatus_Grupo:          number,
    Estatus_Correo:         number,
    Quitar_Usuario:         string
}