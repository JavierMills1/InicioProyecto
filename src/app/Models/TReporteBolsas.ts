export interface ReporteBolsas{
    Aviso:                      string,
    Cve_Programa:               number,
    Estatus:                    string,
    Programa:                   string,
    Fecha_Liberacion:           string,
    Concedido:                  number,
    Dispuesto:                  number,
    Porcentaje_Dispuesto:       number,
    Disponible:                 number,
    Porcentaje_Disponible:      number,
    Detalle:                    string
}