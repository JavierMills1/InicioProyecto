export interface SolicitudesRealizadas{
        Folio:                  string,
        Firma_Contrato:         string,
        Cve_Programa:           number,
        No_Credito:             string,
        Comision:               number,
        Cve_Nafinet:            string,
        Estatus:                string,
        Fecha_Solictado:        string,
        Dias:                   number,
        RFC_Acred:              string,
}