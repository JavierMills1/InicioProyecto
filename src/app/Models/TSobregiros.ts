export interface Sobregiros{
    Fecha:              string,
    Garantia:           number,
    Importe_Linea:      number,
    Saldo_Dia:          number,
    Sobregiro:          string,
    Modificar:          string
}