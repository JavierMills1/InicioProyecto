export interface TipoCambio{
    id:                 number,
    Moneda:             string,
    Valor:              number,
    Accion:             ""
}