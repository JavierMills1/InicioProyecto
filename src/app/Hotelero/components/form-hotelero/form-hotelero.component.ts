import { Component, OnInit } from '@angular/core';
import { SteperService } from 'src/app/AsesorComex/components/steper/steper.service';

@Component({
  selector: 'app-form-hoteleroSolicitud',
  templateUrl: './form-hotelero.component.html',
  styleUrls: ['./form-hotelero.component.scss']
})
export class FormHoteleroComponent implements OnInit {

  mostrarComentario:boolean = false;
  comentario:string = "";
  fechacompleta:any = "";
  alert:boolean = false;
  show: boolean = false;
  public CapturarSolicitud : string = "Capturar Solcitud"
  public ProcesoPendienteAsesorC : string = "Proceso Pendiente Asesor Comex"
  public EspecialistaComexRech : string = "Especialista Comex (Rechazos)"
  public ProcesoPendienteContraloria : string = "Proceso Pendiente Contraloría"
  public FondosDeFomento : string = "Fondos de Fomento"
  public EnviadaNafinet : string = "Enviada a Nafinet"
  public AprobadoNafinet : string = "Aprobado Nafinet"
  public RechaxoNafinet : string = "Rechazo Nafinet"
  public Reproceso : string = "Reproceso"

  fecha: Date = new Date();


  esconder: boolean = false;
  esconderF: boolean = true;
  esconderT: boolean = false;
  showModal: boolean = false;
  buc: string = "";
  nombreORazonSocial: string = "";
  ciudad: string = "";
  colonia: string = "";
  calleNumero: string = "";
  cp: string = "";
  segmento: string = "";
  sexo: string = "";
  entidadFed:string = "Seleccionar";
  munODele:string = "Seleccionar";

  mostrarSolicitudAutomatica: boolean = false;
  mostrarSolicitudSelectiva: boolean = false;
  mostrarSolicitudComexTMEC: boolean = false;
  mostrarContratoPyme: boolean = false;
  mostrarSolicitudTMEC: boolean = false;
  mostrarSolicitudTurismo: boolean = false;



  ventasTA: string = "";
  personalOcupado: string = "";
  sectorGeneral: string = "Seleccionar";
  segmentoBancomext: string = "";
  antiguedadAcreditado: string = "";
  propositoProyecto:string = "Seleccionar";
  porcentajeNacional: string = "";
  personaFisicaMoral:string = "Seleccionar";
  sexoAnexoInfo:string = "Seleccionar";
  actividadEconomica:string = "Seleccionar";
  porcentajeMercadoInterno: string = "";
  vigenciaTotalLinea:string = "";
  montoLinea:string = "";
  tipoCredito:string = "Seleccionar";
  destinoRecursos:string ="Seleccionar";
  codigoBien:string = "Seleccionar";
  tipoGarantia:string = "Seleccionar";
  entidadFinanciera:string = "Seleccionar";
  moneda:string ="Seleccionar";
  // datosnuevos
  noGarantia:string ="";
  spread:string ="";
  tipoGarantia2 ="";
  comisionApertura="";
  regional="";
  noContrato="";
  rfc="";
  topeMaximoC="";
  porcentajeOrigenImportacion="";
  porcentajeMercadoExterno="";
  date="";
  porcentajeGarantia="";
  montoGarantizado="";
  noGarantia2="";
  montoGarantizado2="";
  garantiaFDF="";
  segtorEspecifico="";
  etiqueta="";
  tipoTasa="";

  steper:any;

  constructor(private steperService:SteperService) {

  }

  ngOnInit(): void {

  }

  mostrarContenido() {
    this.show = true;
  }

  closeAlert(){
    this.alert = false;
  }

  MostrarAlert(){
    this.steper = this.steperService.setActive("paso4");
    this.alert = true;
  }


}
