import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HoteleroComponent } from './pages/hotelero/hotelero.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { AsesorComexModule } from '../AsesorComex/asesor-comex.module';
import { RouterModule } from '@angular/router';
import { FormHoteleroComponent } from './components/form-hotelero/form-hotelero.component';



@NgModule({
  declarations: [
    NavbarComponent,
    HoteleroComponent,
    FormHoteleroComponent
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    FormsModule,
    SharedModule,
    AsesorComexModule,
    RouterModule,

  ]
})
export class HoteleroModule { }
